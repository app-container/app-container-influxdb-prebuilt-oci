source ../container-name.sh
IMAGE_NAME=$1
NETWORK_INTERFACE=$2

PUBLIC_PORT="8086"

TAG="latest"

if [ $# -lt 2 ];
then
    echo "+ $0: Too few arguments!"
    echo "+ use something like:"
    echo "+ $0 <docker image> <network interface>"
    echo "+ $0 yoctotrainer/${CONTAINER_NAME}:${TAG} docker0"
    echo "+ $0 yoctotrainer/${CONTAINER_NAME}:${TAG} br0"
    exit
fi

# remove currently running containers
echo "+ ID_TO_KILL=\$(docker ps -a -q  --filter ancestor=$1)"
ID_TO_KILL=$(docker ps -a -q  --filter ancestor=$1)

set -x
docker ps -a
docker stop ${ID_TO_KILL}
docker rm -f ${ID_TO_KILL}
docker ps -a
set +x

set -x
docker pull ${IMAGE_NAME}
set +x

echo "+ ID=\$(docker run -t -i -d -p ${PUBLIC_PORT}:8086 ${IMAGE_NAME})"
ID=$(docker run -t -i -d -p ${PUBLIC_PORT}:8086 ${IMAGE_NAME})

echo "+ ID ${ID}"

# let's attach to it:
echo "+ docker attach ${ID}"
docker attach ${ID}
