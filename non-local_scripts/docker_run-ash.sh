source ../container-name.sh
IMAGE_NAME=$1
NETWORK_INTERFACE=$2

PUBLIC_PORT="8086"

TAG="latest"

if [ $# -lt 2 ];
then
    echo "+ $0: Too few arguments!"
    echo "+ use something like:"
    echo "+ $0 <docker image> <network interface>"
    echo "+ $0 yoctotrainer/${CONTAINER_NAME}:${TAG} docker0"
    echo "+ $0 yoctotrainer/${CONTAINER_NAME}:${TAG} br0"
    exit
fi

# remove currently running containers
echo "+ ID_TO_KILL=\$(docker ps -a -q  --filter ancestor=$1)"
ID_TO_KILL=$(docker ps -a -q  --filter ancestor=$1)

echo "+ docker ps -a"
docker ps -a
echo "+ docker stop ${ID_TO_KILL}"
docker stop ${ID_TO_KILL}
echo "+ docker rm -f ${ID_TO_KILL}"
docker rm -f ${ID_TO_KILL}
echo "+ docker ps -a"
docker ps -a

# start ash shell - need to start lighttpd manually
set -x
docker pull ${IMAGE_NAME} 
docker run -p ${PUBLIC_PORT}:8086 --interactive --tty --entrypoint=/bin/ash ${IMAGE_NAME} --login
set +x

