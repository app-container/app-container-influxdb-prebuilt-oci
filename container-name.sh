#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR="$(basename $DIR)"
CONTAINER_NAME="$DIR"
IMAGE_NAME="app-container-image-influxdb-prebuilt-oci"
